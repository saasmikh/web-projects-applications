# Социальная сеть "Довод" 
## Автор
P3222 Сизов Михаил Сергеевич

## Описание
Социальная сеть, основанная на концепции фильма "Довод". Пользователи сети могут двигаться как вперёд, так и назад во времени, тем самым имея возможность отправлять сообщения и публиковать записи в прошлом. У каждого пользователя отслеживается время, в котором он пребывает и из которого отправляет сообщения, а ему не показываются сообщения, оставленные в будущем для него.

## Детали
- Продукт - веб-приложение c базой данных, пользовательский интерфейс - веб-страницы.
- Стек технологий - Spring MVC, MySQL, Thymeleaf.